const express = require('express');
const socketIO = require('socket.io');
const http = require("http");
const path = require('path');
const { Usuarios } = require("./classes/usuarios");
const { crearMensaje } = require("./utilidades/utilidades");
const app = express();

let server = http.createServer(app);

const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;

app.use(express.static(publicPath));

//IO esta es la comunicacion del backend
let io = socketIO(server);

const usuarios = new Usuarios();

const users = {}

io.on("connection", (client) => {
    console.log("Usuario conectado");

    /*client.on("entrarChat", (usuario) =>
        console.log("Usuario conectado ", usuario)
    );*/

    client.on("entrarChat", (data, callback) => {
        if (!data.nombre) {
            return callback({
                error: true,
                mensaje: "El nombre es necesario",
            });
        }
        //Para unir a una sala
        client.join(data.sala);

        usuarios.agregarPersona(client.id, data.nombre, data.sala);

        client.broadcast.to(data.sala).emit("listaPersonas", usuarios.getPersonasPorSala(data.sala));
        io.sockets.emit('listaPersonas', usuarios.getPersonas());
        /*let personas = usuarios.agregarPersona(client.id, data.nombre);

        client.broadcast.emit("listaPersonas", usuarios.getPersonas());
        //callback(personas);

        io.sockets.emit('update', usuarios.getPersonas());
        return callback({personas}); */
    });

    client.on("crearMensaje", (data) => {
        let persona = usuarios.getPersona(client.id)
        let mensaje = crearMensaje(persona.nombre, data.mensaje);
        client.broadcast.emit("crearMensaje", mensaje);
    })

    client.on("disconnect", () => {
        let personaBorrada = usuarios.borrarPersona(client.id);

/*         client.broadcast.emit("crearMensaje", {
            usuario: "Administrador",
            mensaje: crearMensaje("Admin", `${personaBorrada.nombre} salio`),
        }); */

        client.broadcast.emit("listaPersonas", usuarios.getPersonas());

        client.broadcast.emit('user-disconnected', users[client.id])
        delete users[client.id]
        //io.sockets.emit('update', usuarios.getPersonas());
    });

    client.on("mensajePrivado", (data) => {
        console.log("Mensaje Privado para: " + data.para)
        //console.log("Mensaje Privado data: " + Object.entries(data))
        console.log("Mensaje Privado mensaje: " + data.mensaje)
        let persona = usuarios.getPersona(client.id);

        client.broadcast.to(data.para).emit("mensajePrivado", crearMensaje(persona.nombre, data.mensaje));
    });
    /*---------------------------------------------------*/
    client.on('new-user', name => {
        users[client.id] = name
        client.broadcast.emit('user-connected', name)
      })
/*       client.on('send-chat-message', message => {
        client.broadcast.emit('chat-message', { message: message, name: users[client.id] })
      }) */
/*       client.on('disconnect', () => {
        client.broadcast.emit('user-disconnected', users[client.id])
        delete users[client.id]
      }) */

    /*client.emit('enviarMensaje',{
        usuario: 'Administrador',
        mensaje: 'Bienvenido a esta aplicacion'
    })

    client.on("disconnect", () => {
        console.log("Usuario desconectado");
    });

    //Escuchar el cliente

    client.on("enviarMensaje", (data, callback) => {
        console.log(data);

        client.broadcast.emit("enviarMensaje", data);
    }); */
});

server.listen(port, (err) => {

    if (err) throw new Error(err);

    console.log(`Servidor corriendo en puerto ${ port }`);

});